import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.io.File;

import static org.junit.Assert.*;

/**
 * Created by violet on 14.06.16.
 */
public class ColorchooserTest {
    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void arguments() throws Exception
    {
        boolean thrown = false;

        try
        {
            final Colorchooser colorchooser = new Colorchooser(".png","", Color.WHITE);
        }
        catch (IllegalArgumentException e)
        {
            thrown = true;
        }

        assertTrue(thrown);

        thrown = false;

        try
        {
            final Colorchooser colorchooser = new Colorchooser("","colorchooser.png", Color.WHITE);
        }
        catch (IllegalArgumentException e)
        {
            thrown = true;
        }

        assertTrue(thrown);

        thrown = false;

        try
        {
            final Colorchooser colorchooser = new Colorchooser(null,"colorchooser.png", Color.WHITE);
        }
        catch (IllegalArgumentException e)
        {
            thrown = true;
        }

        assertTrue(thrown);

        thrown = false;

        try
        {
            final Colorchooser colorchooser = new Colorchooser(null,"colorchooser.png", Color.WHITE);
        }
        catch (IllegalArgumentException e)
        {
            thrown = true;
        }

        assertTrue(thrown);
    }

    @Test
    public void process() throws Exception
    {
        File fbad = new File("./testdata_bad/product/colorchooser.png");

        if(fbad.exists())
            fbad.delete();

        File fgood = new File("./testdata_good/product/colorchooser.png");

        if(fgood.exists())
            fgood.delete();

        boolean thrown = false;

        try
        {
            final Colorchooser colorchooser = new Colorchooser(".png","colorchooser.png", Color.WHITE);

            colorchooser.Process("./testdata_bad");
        }
        catch (Exception e)
        {
            thrown = true;
        }

        assertFalse(thrown);

        assertTrue(fbad.exists());

        thrown = false;

        try
        {
            final Colorchooser colorchooser = new Colorchooser(".png","colorchooser.png", Color.WHITE);

            colorchooser.Process("./testdata_good");
        }
        catch (Exception e)
        {
            thrown = true;
        }

        assertFalse(thrown);

        assertTrue(fgood.exists());


    }


}