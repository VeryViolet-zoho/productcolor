import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Objects;

public class Runner
{
    public static void main(String[] args)
    {
        System.out.println("ProductColor v0.1\n\n");

        if(args.length < 2)
        {
            System.out.println("usage:  ProductColor [c|p] [source-folder] \n\n");
            System.out.println("\t\t c - choose color representative.\n");
            System.out.println("\t\t p - choose pattern representative.\n");
            System.out.println("\t\t b - clean background.\n");
            return;

        }

        SubimageChooser chooser = null;

        if(Objects.equals(args[0], "c"))
            chooser = new Colorchooser(".png","colorchooser.png", Color.WHITE);
        else if(Objects.equals(args[0], "p"))
            chooser = new Patternchooser(".png","colorchooser.png", Color.WHITE);
        else
        {
            System.out.println("--> Wrong method parameter!\n\n");
            return;
        }

        try {
            chooser.Process(args[1]);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
