import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;


class Patternchooser extends SubimageChooser
{
    private final int textureBin = 16;

    private Map<Integer, Double> Histogram;

    private int totalPixels;

    private void CalculateHistogram()
    {
        for(BufferedImage im: images)
        {
            int width = im.getWidth();
            int height = im.getHeight();

            for(int i=0;i<width;i++)
            {
                for(int j=0;j<height;j++)
                {
                    int rgb = im.getRGB(i,j);
                    Color c = new Color(rgb);

                    if(ColorDistance(c, backgroundColor) < maxDistanceToBackground)
                        continue;

                    Color bin = new Color(c.getRed()-(c.getRed() % textureBin),
                            c.getGreen()-(c.getGreen() % textureBin),
                            c.getBlue()-(c.getBlue() % textureBin));



                    if(Histogram.containsKey(bin))
                    {
                        Histogram.put(rgb, Histogram.get(rgb)+1.0);
                    }
                    else
                    {
                        Histogram.put(rgb,1.0);
                    }


                    totalPixels++;
                }
            }

        }

    }

    @Override
    BufferedImage ChooseRepresentative()
    {

        return null;
    }

    Patternchooser(String imExt, String repFileName, Color backColor)
    {
        super(imExt,repFileName,backColor);

        Histogram = new HashMap<Integer, Double>();
    }
}
