import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import org.apache.log4j.Logger;

abstract class SubimageChooser
{
    final String imageExtension;
    final String representativeFileName;
    final Color backgroundColor;

    ArrayList<BufferedImage> images;

    int bestImageIndex;
    int bestX;
    int bestY;

    // Constants that determine histogram bins and the size of generated
    // color chooser image.
    final int Bins = 256;
    final int reprWidth = 30;
    final int reprHeight = 30;

    // Pixels which color distance to background color is less than maxDistanceToBackground would be
    // considered background
    final int maxDistanceToBackground = 35;


    final static Logger logger = Logger.getLogger(Colorchooser.class);

    SubimageChooser(String imExt, String repFileName, Color backColor)
    {
        if(imExt == null)
            throw new IllegalArgumentException("image extention must not be null");

        if(imExt.length() == 0)
            throw new IllegalArgumentException("image extention is empty!");

        if(repFileName == null)
            throw new IllegalArgumentException("colorchooser filename must not be null");

        if(repFileName.length() == 0)
            throw new IllegalArgumentException("colorchooser filename is empty!");

        if(backColor == null)
            throw new IllegalArgumentException("background color must not be null");

        this.imageExtension = imExt;
        this.representativeFileName = repFileName;
        this.backgroundColor = backColor;

        images = new ArrayList<>();
    }

    void LoadImages(File fld)
    {
        images.clear();

        File[] files = fld.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(imageExtension);
            }
        });

        for(File f: files)
        {
            try
            {
                BufferedImage readImage = ImageIO.read(f);

                if(readImage == null)
                    continue;

                images.add(ImageIO.read(f));

            } catch (IOException e)
            {
                continue;
            }

        }

    }

    void SaveRepresentative(BufferedImage im, File fld) throws IOException
    {
        ImageIO.write(im,"png",new File(fld + File.separator + representativeFileName));
    }

    boolean IsRepresentativeExists(File fld)
    {
        File[] repr = fld.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return Objects.equals(name, representativeFileName);
            }
        });

        return repr.length  == 1;
    }

    void ProcessProduct(File fld) throws IOException
    {
        if(IsRepresentativeExists(fld))
            return;

        LoadImages(fld);

        SaveRepresentative(ChooseRepresentative(), fld);
    };

    abstract BufferedImage ChooseRepresentative();


    // Main public method. source must specify folders, where each
    int Process(String source) throws Exception
    {
        File sourceFolder = new File(source);

        if(!sourceFolder.isDirectory())
            throw(new Exception("source must be a folder!"));

        File[] subFolders = sourceFolder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });

        for(File f: subFolders)
        {
            logger.info(String.format("--> Processing product: %s.", f.toString()));
            ProcessProduct(f);
        }

        return subFolders.length;
    };

    double ColorDistance(Color c1, Color c2)
    {
        double rd = ((double)c1.getRed())-((double)c2.getRed());
        double gd = ((double)c1.getGreen())-((double)c2.getGreen());
        double bd = ((double)c1.getBlue())-((double)c2.getBlue());

        return Math.sqrt(rd*rd+gd*gd+bd*bd);
    }


}
