import java.awt.*;
import java.awt.image.BufferedImage;


class Colorchooser extends SubimageChooser
{




    private double r[];
    private double g[];
    private double b[];
    private int totalPixels;


    Colorchooser(String imExt, String repFileName, Color backColor)
    {
        super(imExt,repFileName,backColor);

        ClearHistogram();
    }


    private void ClearHistogram()
    {
        r = new double[Bins];
        g = new double[Bins];
        b = new double[Bins];

        totalPixels = 0;
    }


    private void CalculateHistograms()
    {
        ClearHistogram();

        for(BufferedImage im: images)
            CalculateOneHistogram(im);

        for(int i=0; i<Bins; i++)
        {
            r[i] /= totalPixels;
            g[i] /= totalPixels;
            b[i] /= totalPixels;
        }
    }

    private void CalculateOneHistogram(BufferedImage im)
    {
        int width = im.getWidth();
        int height = im.getHeight();

        for(int i=0;i<width;i++)
        {
            for(int j=0;j<height;j++)
            {
                Color c = new Color(im.getRGB(i,j));

                if(ColorDistance(c, backgroundColor) < maxDistanceToBackground)
                    continue;

                r[c.getRed()] += 1.0;
                g[c.getGreen()] += 1.0;
                b[c.getBlue()] += 1.0;
                totalPixels++;
            }
        }
    }

    private double GetProbability(Color c)
    {
        if(ColorDistance(c, backgroundColor) < maxDistanceToBackground)
            return 0.0;

       return (r[c.getRed()] * g[c.getGreen()] * b[c.getBlue()]);
    }

    private double[][] CalculateIntegralImage(BufferedImage im)
    {
        int width = im.getWidth();
        int height = im.getHeight();

        double[][] Integral = new double[width][height];

        Integral[0][0] = GetProbability(new Color(im.getRGB(0,0)));

        for(int x = 1; x < width; x++)
            Integral[x][0] = Integral[x-1][0] + GetProbability(new Color(im.getRGB(x,0)));

        for(int y = 1; y < height; y++)
            Integral[0][y] = Integral[0][y-1] + GetProbability(new Color(im.getRGB(0,y)));


        for(int x = 1; x < width; x++)
        {
            for (int y = 1; y < height; y++)
            {
                Integral[x][y] = GetProbability(new Color(im.getRGB(x,y))) +
                        Integral[x-1][y]+Integral[x][y-1]-Integral[x-1][y-1];
            }
        }

        return Integral;
    }

    private BufferedImage SearchForRepresentativeIntegral()
    {
        double maxProbability = 0.0;
        bestImageIndex = 0;
        bestX = 0;
        bestY = 0;

        for(int i=0; i<images.size(); i++)
        {
            BufferedImage im = images.get(i);
            int width = im.getWidth();
            int height = im.getHeight();

            double[][] Integral = CalculateIntegralImage(im);


            for(int x = 0; x < width-reprWidth; x++)
            {
                for(int y = 0; y < height-reprHeight; y++)
                {
                    double probability = Integral[x][y] + Integral[x+reprWidth-1][y+reprHeight-1]
                            - Integral[x][y+reprHeight-1] - Integral[x+reprWidth-1][y];

                    if(probability > maxProbability)
                    {
                        maxProbability = probability;
                        bestX = x;
                        bestY = y;
                        bestImageIndex = i;
                    }

                }
            }
        }

        return images.get(bestImageIndex).getSubimage(bestX,bestY,reprWidth,reprHeight);
    }

    @Override
    BufferedImage ChooseRepresentative()
    {
        CalculateHistograms();

        return SearchForRepresentativeIntegral();
    }


}
